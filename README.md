# ConstantRe-pair
Player controls two entities that fulfil movement and combat roles. They feel a constant force of attraction to each other. They protect each other, and solve problems together.
