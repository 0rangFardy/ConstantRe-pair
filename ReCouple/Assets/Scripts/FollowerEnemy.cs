﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowerEnemy : DamageTaker
{
    public PlayerController player;
    public FollowerMovement followerPlayer;
    public static float radius = 0.3f;
    public int pathdistance;

    public EnemyQueen thisQueen;
    protected override void OnDie()
    {
        Destroy(gameObject);
        if (thisQueen != null)
        {
            thisQueen.spawned--;
        }
        UiControllers.killFollower();
    }

    // Start is called before the first frame update
    void Start()
    {
        UiControllers.LiveFollowers++;
    }

    // Update is called once per frame
    void Update()
    {

    }
    Vector3 offsetToClosestPlayer()
    {
        Vector3 diffPlayer = player.transform.position - transform.position;
        Vector3 diffFollow = followerPlayer.transform.position - transform.position;
        if (diffPlayer.magnitude < diffFollow.magnitude)
        {
            return diffPlayer;
        }
        else
        {
            return diffFollow;
        }
    }

    void moveTowardsPlayer(Vector3 closest)
    {
        transform.position += closest.normalized / 10;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 closest = offsetToClosestPlayer();
        if (closest.magnitude < pathdistance)
        {
            moveTowardsPlayer(closest);
        }

    }
}
