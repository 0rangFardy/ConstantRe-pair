﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UiControllers : MonoBehaviour
{
    public static int LiveQueens = 0;
    public static int LiveFollowers = 0;

    public static void killQueen()
    {
        LiveQueens--;
        testNumber();
    }
    public static void killFollower()
    {
        LiveFollowers--;
        testNumber();
    }
    private static void testNumber()
    {
        if (LiveQueens == 0 && LiveFollowers == 0)
        {
            SceneManager.LoadScene("VictoryScene");
        }
    }

    public void NextScene(string scenename)
    {
        SceneManager.LoadScene(scenename);
    }
}
