﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class samorientationlead : MonoBehaviour
{
	public Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		if(rb.velocity.magnitude>0.2)
         transform.rotation = Quaternion.LookRotation(rb.velocity);
    }
}
