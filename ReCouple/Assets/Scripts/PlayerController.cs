﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class PlayerController : MonoBehaviour
{

    public int health;
    public float speed;
    //public Text countText;
    //public Text winText;

    private Rigidbody rb;


    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        //SetCountText();
        //winText.text = "";
    }

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            DamageTaker damage = other.GetComponent<DamageTaker>();
            damage.TakeDamage(2);
            TakeDamage();
        }
    }
    void TakeDamage(int amount = 1)
    {
        this.health -= amount;
        if (this.health == 0)
        {
            SceneManager.LoadScene("GameOverScene");
        }
    }

    /*void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 12)
        {
            winText.text = "You Win!";
        }
    }*/

}
