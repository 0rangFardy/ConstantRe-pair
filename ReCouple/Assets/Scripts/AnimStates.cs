﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimStates : MonoBehaviour
{
    public bool lead;
	public Rigidbody rb;
	private Animator anim;
	private int count;
    // Start is called before the first frame update
    void Start()
    {
        anim=GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0)&&!lead)
			anim.SetBool("attack",true);
        if(rb.velocity.magnitude<1)
			anim.SetBool("walk",false);
		else
			anim.SetBool("walk",true);
    }
}
