﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyQueen : DamageTaker
{
    public static float radius = 0.5f;
    public FollowerEnemy follower;
    public PlayerController player;
    public FollowerMovement followerPlayer;

    public int distanceToSpawn;
    public int maxSpawned;
    public float timebetween;
    public int spawned = 0;
    private float timeLastSpawned;

    // Start is called before the first frame update
    void Start()
    {
        UiControllers.LiveQueens++;
    }

    protected override void OnDie()
    {
        Destroy(gameObject);
        UiControllers.killQueen();
    }


    Vector3 offsetToClosestPlayer()
    {
        Vector3 diffPlayer = player.transform.position - transform.position;
        Vector3 diffFollow = followerPlayer.transform.position - transform.position;
        if (diffPlayer.magnitude < diffFollow.magnitude)
        {
            return diffPlayer;
        }
        else
        {
            return diffFollow;
        }
    }

    void SpawnFollower()
    {
        FollowerEnemy enemy = Instantiate(follower, transform.position + new Vector3(Random.Range(-3, 3), FollowerEnemy.radius, Random.Range(-3, 3)), Quaternion.identity);
        spawned++;
        timeLastSpawned = Time.time;
        enemy.player = player;
        enemy.followerPlayer = followerPlayer;
        enemy.thisQueen = this;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (spawned < maxSpawned && timeLastSpawned + timebetween < Time.time)
        {
            if (offsetToClosestPlayer().magnitude < distanceToSpawn)
            {
                SpawnFollower();
            }
        }
    }
}
