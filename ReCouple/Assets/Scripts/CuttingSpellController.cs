﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CuttingSpellController : MonoBehaviour
{
  Image myImageComponent;
 public Sprite myFirstImage; //Drag your first sprite here in inspector.
 public Sprite mySecondImage; //Drag your second sprite here in inspector.

 void Start() //Lets start by getting a reference to our image component.
 {
 myImageComponent = GetComponent<Image>(); //Our image component is the one attached to this gameObject.
 }

 public void SetImage1() //method to set our first image
 {
 myImageComponent.sprite = myFirstImage;
 }

 public void SetImage2(){
 myImageComponent.sprite = mySecondImage;
 }

    // Update is called once per frame
    void Update()
    {
      if (Input.GetKeyDown("2"))
        {
            SetImage2();
        }
      if (Input.GetKeyUp("2"))
        {
            SetImage1();
        }
      //gameObject.GetComponent<Image>().sprite=FireballClear;
    }
}
