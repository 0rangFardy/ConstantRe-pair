﻿using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;


public class FollowerMovement : MonoBehaviour
{
    public int health;
    public Resources resources;
    public float speed;
    public float power;
    public float mindist; //distance at which gradual deceleration is applied
    public float stopdist; //absolute minimum distance where momentum is stopped
    public float maxfollow;
    public PlayerController playerController;
    public Rigidbody projectileRb;
    public float projectileSpeed;


    private Vector3 movement;
    private Rigidbody playerSphere;
    private Rigidbody rb;
    private Vector3 aimDirection; //direction to shoot at
    private Quaternion rotation;

    public Vector3 AimDirection
    {
        get
        {
            return aimDirection;
        }
    }

    public Vector3 Movement
    {
        get
        {
            return movement;
        }
    }

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        playerSphere = playerController.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                Vector3 vectorToMouseDown = hit.point - transform.position;
                aimDirection = vectorToMouseDown.normalized;

            }

            Shoot();
        }
    }

    void FixedUpdate()
    {
        HandleMovement();
    }

    void HandleMovement()
    {
        movement = playerSphere.position - rb.position; //vector from follower to player
        if (movement.magnitude < stopdist)
        {
            rb.velocity *= 0;
        }
        else if (movement.magnitude < mindist)
        {
            rb.velocity *= 0.9f;
        }
        else if (movement.magnitude > maxfollow)
        {
            rb.velocity *= 0;
        }
        else
        {
            rb.velocity *= 0.95f;

            movement *= (float)(Math.Pow(movement.magnitude, power));
            rb.AddForce(movement * speed);
        }
    }

    void Shoot()
    {
        Vector3 offset = aimDirection;
        Rigidbody shot = Instantiate(projectileRb, transform.position + offset, Quaternion.identity) as Rigidbody;
        shot.velocity = new Vector3(aimDirection.x * projectileSpeed, 0f, aimDirection.z * projectileSpeed);
        //Vector3 projectileVelocity = new Vector3(spawnVelocity.x * direction, spawnVelocity.y);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            DamageTaker damage = other.GetComponent<DamageTaker>();
            damage.TakeDamage(2);
            TakeDamage();
        }
    }
    void TakeDamage(int amount = 1)
    {
        this.health -= amount;
        if (this.health == 0)
        {
            SceneManager.LoadScene("GameOverScene");
        }
    }
}
