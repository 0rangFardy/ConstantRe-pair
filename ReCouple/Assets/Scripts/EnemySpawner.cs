﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public PlayerController player;
    public FollowerMovement followerPlayer;
    public EnemyQueen queen;
    public FollowerEnemy followerEnemy;
    public Renderer ground;
    public int numQueens;
    public int numQueenless;


    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < numQueens; i++)
        {
            EnemyQueen newQueen = SpawnEnemy(queen, EnemyQueen.radius, 0);
            newQueen.followerPlayer = followerPlayer;
            newQueen.player = player;
        }
        for (int i = 0; i < numQueenless; i++)
        {
            FollowerEnemy newFollower = SpawnEnemy(followerEnemy, FollowerEnemy.radius, FollowerEnemy.radius);
            newFollower.followerPlayer = followerPlayer;
            newFollower.player = player;
        }
    }

    Vector3 SpawnPosition(float radius, float y)
    {
        float x = Random.Range(ground.bounds.min.x + radius * 1.5f, ground.bounds.max.x - radius * 1.5f);
        float z = Random.Range(ground.bounds.min.z + radius * 1.5f, ground.bounds.max.z - radius * 1.5f);
        return new Vector3(x, y, z);
    }

    T SpawnEnemy<T>(T type, float radius, float y) where T : class
    {
        return Instantiate(type as Object, SpawnPosition(radius, y), Quaternion.identity) as T;
    }


    // Update is called once per frame
    void Update()
    {
    }
}
