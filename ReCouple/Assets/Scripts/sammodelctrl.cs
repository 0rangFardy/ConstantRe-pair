﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sammodelctrl : MonoBehaviour
{	
	public FollowerMovement followerMovement;
	private Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        anim=GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(!anim.GetBool("attacke")){
            transform.rotation=Quaternion.LookRotation(followerMovement.AimDirection);
            return;
        }
        transform.rotation = Quaternion.LookRotation(followerMovement.Movement);
    }
}
