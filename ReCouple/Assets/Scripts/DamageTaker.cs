using UnityEngine;

public class DamageTaker : MonoBehaviour
{
    public int Health;

    public void TakeDamage(int amount = 1)
    {
        Health--;
        if (Health <= 0)
        {
            OnDie();
        }
    }
    protected virtual void OnDie() { Destroy(gameObject); }
}